![](https://assets.gitlab-static.net/uploads/-/system/project/avatar/13095639/FPS_Icon.png)
# Fire Players Stats - Плагин статистики для серверов

## Информация о статистике:

 - Поддержка только **CS:GO**.
 - Статистика работает на основе формулы **ELO [Levels Ranks](https://github.com/levelsranks/levels-ranks-core)**. Суть его в том, что вы получаете 1000 очков опыт и после калибровки среднее звание. В зависимости от того, насколько хорошо вы играете зависит ваше звание.
 - Статистика работает только с **MySQL** и рассчитана на работу с **[WEB Interface](https://hlmod.ru/resources/levels-ranks-actual-adaptive-web.1291/)** для LR, который адаптирован под FPS!
 - Количество рангов не ограничено. Настройка производится через команду **sm_fps_create_default_ranks** или в ручную, отправив SQL запрос.
 - Совмещенная база данных для нескольких серверов (по принципу випки от Рико) с нормальной структурой.
 - Статистика по оружию хранится в отдельной таблице, из-за чего при выходе нового оружия изменять плагин и БД не придется.
 - Статистика пытается исправить превосходство новых игроков перед старыми при расчете поинтов.
 - КиллСтрик: Начисление дополнительных поинтов идет в течении 10 сек после убийства, после чего идет обнуление.
 - Возможна установка лимита на обнуление статистики по времени для пользователя.
 - Информация о полученных/потерянных поинтах выводится только в конце раунда, подводя итог раунда.
 - Значения поинтов хранится в float.
 - Плагин поддерживает возможность сделать перевод рангов.
 - Плагин поддерживает бонус за убиство с кокретного оружия с учетом карты (Можно указывать разный для разных карт).

### Важно понимать, что статистика по функционалу очень схожа с ЛР, ибо та бралась за основу, но плагин писался полностью с нуля! Он лишен возможных болячек ЛРа, но может иметь свой букетик!

<!-- <details><summary>Меню плагина</summary> </details> -->

 [**_Список модулей к статистике_**](https://gitlab.com/OkyHp/fire-players-stats/tree/master/FPS_Modules)

 > Спасибо за идеии в реализации: [Разработчикам LR](https://github.com/orgs/levelsranks/people), [Someone](https://hlmod.ru/members/someone.73313/).

## Команды плагина:

### Для игроков:

**_sm_pos_**, **_sm_position_** - Позиция игрока на сервере. \
**_sm_stats_**, **sm_rank**, **sm_fps** - Главное меню статистики. \
**_sm_top_** - Список доступных топов. При использовании аргументов points, kdr, time, clutch открывается соответственный топ.​

### Для администратора:

**_sm_fps_create_default_ranks_** - Создание настройки рангов.\
	⋅⋅⋅ **0** - Стандартные ранги CS:GO Competitive (18 lvl)\
 	⋅⋅⋅ **1** - Ранги опасной зоны (15 lvl)\
 	⋅⋅⋅ **2** - Фейсит ранги (10 lvl)

## Требования

  - [**Sourcemod 1.9+**](https://www.sourcemod.net/downloads.php?branch=stable)
  - [**SteamWorks**](http://users.alliedmods.net/~kyles/builds/SteamWorks/)

## Установка

 1. Скачайте актуальную версию с репозитория.
 2. Поместите содержимое архива по нужным директориям.
 4. Добавьте секцию с вашими настройками БД в `addons/sourcemod/configs/databases.cfg`:
	```
	"fire_players_stats"
	{
		"driver"			"mysql"
		"host"				""
		"database"			""
		"user"				""
		"pass"				""
		"port"				"3306"
	}
	```
 5. Запустите сервер, чтобы плагин создал нужные таблицы в БД.
 6. Введите команду `sm_fps_create_default_ranks`, чтобы использовать предустановленные настройки.\
 		**0** - Стандартные ранги CS:GO Competitive (18 lvl),\
 		**1** - Ранги опасной зоны (15 lvl),\
 		**2** - Фейсит ранги (10 lvl),
 	<details><summary>Или загрузите настройку рангов в ручную, отправив SQL запрос в БД, предварительно откорректировав его под ваши нужды:</summary>

	```sql
	INSERT INTO `fps_ranks` (`rank_id`, `rank_name`, `points`) 
	VALUES 
		('1', 'Silver I',				'0'),
		('1', 'Silver II',				'700'), 
		('1', 'Silver III',				'800'), 
		('1', 'Silver IV',				'850'), 
		('1', 'Silver Elite',				'900'), 
		('1', 'Silver Elite Master',			'925'), 
		('1', 'Gold Nova I',				'950'), 
		('1', 'Gold Nova II',				'975'), 
		('1', 'Gold Nova III',				'1000'), 
		('1', 'Gold Nova Master',			'1100'), 
		('1', 'Master Guardian I',			'1250'), 
		('1', 'Master Guardian II',			'1400'), 
		('1', 'Master Guardian Elite',			'1600'), 
		('1', 'Distinguished Master Guardian',		'1800'), 
		('1', 'Legendary Eagle',			'2100'), 
		('1', 'Legendary Eagle Master',			'2400'), 
		('1', 'Supreme Master First Class',		'3000'), 
		('1', 'The Global Elite',			'4000');
	```

	</details>

**Тема на [HLMOD](https://hlmod.ru/resources/fire-players-stats.1232/)**.
**[Discord server](https://discord.gg/M82xN4y)**.
